package mx.tec.clase2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts

class MainActivity : AppCompatActivity() {

    lateinit var editText : EditText

    // ActivityResultLauncher
    val lanzador = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->

        // qué pasa cuando tengo resultado
        if(result.resultCode == Activity.RESULT_OK){

            // obtener primero el intent
            val datos = result.data

            // regresa Intent?
            // existen tipos nullable y non-nullable
            // lo que venga de java lo más probable es nullable

            // hacer algo con la info que me regresen!

            // safecall (checa datos?)
            Toast.makeText(this, "regresando de aplicacion ${datos?.getStringExtra("regreso1")}" , Toast.LENGTH_SHORT).show()

            /*
            // alternativa 1
            if(datos != null)
                Toast.makeText(this, "regresando de aplicacion ${datos.getStringExtra("regreso1")}" , Toast.LENGTH_SHORT).show()

            // alternativa 2 (tratar de no usar)
            Toast.makeText(this, "regresando de aplicacion ${datos!!.getStringExtra("regreso1")}" , Toast.LENGTH_SHORT).show()


            */

            // alternativa 3 (elvis operator)
            // ?:
            // Toast.makeText(this, datos?: "regresando de aplicacion ${datos.getStringExtra("regreso1")}" , Toast.LENGTH_SHORT).show()
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // comentarios como java

        // escribir al log
        Log.d("TESTLOG", "Log de debug")
        Log.i("TESTLOG", "Log de info")
        Log.w("TESTLOG", "Log de warning")
        Log.e("TESTLOG", "Log de error")
        Log.wtf("TESTLOG", "what a terrible failure!")

        // toast
        Toast.makeText(this, "HOLA DESDE EL TOAST", Toast.LENGTH_SHORT).show()

        // variable mutable
        var saludo : TextView = findViewById(R.id.saludo)
        editText = findViewById<EditText>(R.id.editText1)

        // variable no mutable
        val button : Button = findViewById(R.id.button)

        saludo.setText("PRUEBITA DESDE CODIGO")
        button.setText("HOLA BOTONCITO")

        val button2 : Button = findViewById(R.id.button2)

        button2.setOnClickListener {

            Log.wtf("TESTLOG", it.toString())
            Toast.makeText(this, "CLICK DE METODO", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onStart() {
        super.onStart()
    }

    // deteccion de click
    public fun clickDetectado(v : View?){

        Toast.makeText(this, "CLICK DE FUNCION", Toast.LENGTH_SHORT).show()

        // cambio de actividad
        // se le solicita al OS el cambio
        // utilizamos un intent como solicitud
        // 2 formas de solicitar una nueva actividad: con tipo explicito
        // por medio de una acción (ej: tomar foto)
        val intent = Intent(this, SecondActivity::class.java)

        intent.putExtra("nombre", editText.getText().toString())
        intent.putExtra("calificacion", 100)

        // startActivity(intent)
        lanzador.launch(intent)
    }
}