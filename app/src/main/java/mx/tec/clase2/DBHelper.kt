package mx.tec.clase2

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

// para db local vamos a usar un dbhelper
// hereda de un dbhelper de Sqlite
// Sqlite - manejador de base de datos local
class DBHelper(context : Context?) :
    SQLiteOpenHelper(context, DB_FILE, null, 1) {

    override fun onCreate(db: SQLiteDatabase?) {

        val query = "CREATE TABLE $TABLE(" +
                "$COLUMN_ID INTEGER PRIMARY KEY, " +
                "$COLUMN_NAME TEXT, " +
                "$COLUMN_AGE INTEGER)"

        db?.execSQL(query)
    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {

        // manejo detallado de transferencia de datos

        // prepared statement - query parametrizada
        val query = "DROP TABLE IF EXISTS ?"
        val args = arrayOf(TABLE)

        db?.execSQL(query, args)
        onCreate(db)
    }

    fun save(nombre : String?, edad : Int){

        val valores = ContentValues()
        valores.put(COLUMN_NAME, nombre)
        valores.put(COLUMN_AGE, edad)

        writableDatabase.insert(TABLE, null, valores)
    }

    fun delete(nombre : String?) : Int {

        // necesitamos una clausula
        // clausula - criterio utilizado para borrar
        val clause = "$COLUMN_NAME = ?"
        val args = arrayOf(nombre)

        return writableDatabase.delete(TABLE, clause, args)
    }

    fun find(nombre : String?) : Int {

        val clause = "$COLUMN_NAME = ?"
        val args = arrayOf(nombre)
        val cursor = readableDatabase.query(TABLE, null, clause, args, null, null, null)

        var result = -1

        if(cursor.moveToFirst())
            result = cursor.getInt(2)

        while(cursor.moveToNext()){
            // recorrido
        }

        return result
    }

    companion object {
        private const val DB_FILE = "DatabasePerritos.db"
        private const val TABLE = "Perritos"
        private const val COLUMN_ID = "id"
        private const val COLUMN_NAME = "nombre"
        private const val COLUMN_AGE = "edad"
    }
}