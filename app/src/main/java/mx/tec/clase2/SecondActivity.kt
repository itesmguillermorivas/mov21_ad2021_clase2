package mx.tec.clase2

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.SeekBar
import android.widget.Toast

class SecondActivity : AppCompatActivity() {

    lateinit var db : DBHelper
    lateinit var id : EditText
    lateinit var nombre : EditText
    lateinit var edad : EditText
    lateinit var prefs: SharedPreferences
    lateinit var seekBar: SeekBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        Toast.makeText(this, intent.getStringExtra("nombre"), Toast.LENGTH_SHORT).show()
        Toast.makeText(this, "${intent.getFloatExtra("calificacion", 0f)}" , Toast.LENGTH_SHORT).show()

        db = DBHelper(this)

        id = findViewById(R.id.segunda_id)
        nombre = findViewById(R.id.segunda_nombre)
        edad = findViewById(R.id.segunda_edad)
        seekBar = findViewById(R.id.seekBar)

        //seekBar.progress
    }

    fun finDeActividad(v: View?){

        val intent = Intent()
        intent.putExtra("regreso1", "hola a todos!")
        intent.putExtra("regreso2", 14.05f)

        // aquí es donde vamos a decir qué fue el resultado
        setResult(Activity.RESULT_OK, intent)
        // setResult(Activity.RESULT_OK)
        finish()
    }

    fun guardarDb(v: View?) {

        db.save(nombre.text.toString(), edad.text.toString().toInt())
        Toast.makeText(this, "GUARDADO", Toast.LENGTH_SHORT).show()
    }
    fun borrarDb(v: View?) {

        var modificadas = db.delete(nombre.text.toString())
        Toast.makeText(this, "se borraron $modificadas renglones", Toast.LENGTH_SHORT).show()
    }

    fun buscarDb(v: View?) {

        var peso = db.find(nombre.text.toString())
        Toast.makeText(this, "el perrito pesa: $peso", Toast.LENGTH_SHORT).show()
    }

    fun cargarPrefs(v : View?) {

        prefs = getSharedPreferences(ARCHIVO, MODE_PRIVATE)
        Toast.makeText(this, "SHARED PREFS CARGADAS", Toast.LENGTH_SHORT).show()
    }

    fun imprimirPrefs(v : View?) {

        Toast.makeText(this,
                        "VALOR DE CAMPO: ${prefs.getString("llavecita", "SIN VALOR")}",
                        Toast.LENGTH_SHORT).show()
    }

    fun guardarPrefs(v : View?) {

        val editor = prefs.edit()
        editor.putString("llavecita", nombre.text.toString())
        editor.commit()
        Toast.makeText(this, "GUARDADO", Toast.LENGTH_SHORT).show()

    }

    fun borrarCampoPrefs(v : View?) {

        val editor = prefs.edit()
        editor.remove("llavecita")
        editor.commit()
        Toast.makeText(this, "CAMPO BORRADO", Toast.LENGTH_SHORT).show()
    }

    fun borrarTodoPrefs(v : View?) {

        val editor = prefs.edit()
        editor.clear()
        editor.commit()
        Toast.makeText(this, "TODO BORRADO", Toast.LENGTH_SHORT).show()
    }

    companion object {

        private const val ARCHIVO = "MisPrefs"
    }
}